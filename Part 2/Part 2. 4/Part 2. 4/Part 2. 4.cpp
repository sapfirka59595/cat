﻿#include <iostream>
using namespace std;

// 4. Написать программу, которая получает 4 числа от пользователя, далее данные числа должны быть сохранены в массив double. 
//    В конце программа должны выводить сумму всех чисел, а также адреса первого и последнего элементов массива

int main()
{
	setlocale(LC_ALL, "Rus");

	int Number1;
	int Number2;
	int Number3;
	int Number4;

	cout << "Введите 4 числа" << endl;

	cin >> Number1;
	cin >> Number2;
	cin >> Number3;
	cin >> Number4;

	double arr[]{ Number1, Number2, Number3, Number4 };

	cout <<"Сумма всех чисел: " << Number1 + Number2 + Number3 + Number4 << endl;

	cout <<"Адрес первого массива: " << "arr[0]" << endl 
		 << "Адрес последнего массива: " << "arr[3]" << endl;
}