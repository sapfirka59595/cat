﻿#include <iostream>
using namespace std;

// 6. Напишите программу, которая получает число от пользователя, а потом вывод 100 следующих чисел от полученного.

int main()
{
	setlocale(LC_ALL, "Rus");

	int a;
	int b = 0;

	cout << "Введите число: ";

	cin >> a;

	for (int i = a; b < 101 ; i++)
	{
		cout << i << endl;
		b++;
	}
}