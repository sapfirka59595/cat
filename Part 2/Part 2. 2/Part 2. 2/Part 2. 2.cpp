﻿#include <iostream>
using namespace std;

// 2. Написать простой калькулятор. Пользователь вводит значение двух переменных. Описать меню в консоли для математических операций сложения, вычитания, умножения, деления. 
//   Через оператор switch реализовать математическую операцию соответствующую пункту меню

int main()
{
	setlocale(LC_ALL, "Rus");

	int var1, var2;
	int a;

	cout << "Введите два числа" << endl;

	cin >> var1;
	cin >> var2;

	cout << "Выберите математическую операцию: " << endl <<
		"1. Сложение" << endl <<
		"2. Вычитание" << endl <<
		"3. Умножение" << endl <<
		"4. Деление" << endl;

	cin >> a;

	switch (a)
	{ 
	case 1:
		cout << "Результат сложения: " << var1 + var2 << endl;
		break;
	case 2:
		cout << "Результат вычитания: " << var1 - var2 << endl;
		break;
	case 3:
		cout << "Результат умножения: " << var1 * var2 << endl;
		break;
	case 4:
		cout << "Результат деления: " << var1 / var2 << endl;
		break;
	default: cout << "Ошибка! Выбрана неизвестная операция!" << endl;
	 break;
	}
}
