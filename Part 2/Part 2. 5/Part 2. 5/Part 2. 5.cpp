﻿#include <iostream>
using namespace std;

// 5. Создайте массив целых чисел 3x3. Заполните массив любыми числами, но так чтобы каждый элемент массива был инциализирован. 
//    Напишите программу, которая запрашивает у игрока 2 числа x и y, в конце должно выводиться значения array[x][y]

int main()
{
	setlocale(LC_ALL, "Rus");

	const int SIZEX = 3;
	const int SIZEY = 3;

	int arr[SIZEX][SIZEY]
	{ 
		{2,7,3},
		{6,9,8} 
	};

	int x;
	int y;

	cout << "Введите число х: ";

	cin >> x;

	cout << "Введите число y: ";

	cin >> y;

	cout << arr[x][y] << endl;
}