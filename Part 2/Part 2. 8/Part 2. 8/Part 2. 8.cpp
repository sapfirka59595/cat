﻿#include <iostream>
#include <ctime>

using namespace std;

// 8. Создайте массив целых чисел из 20 элементов и заполните его рандомными числами от 0 до 100. 
// Напишите программу, которая получает число от 0 до 100 от пользователя и выводит в консоль есть ли это число в массиве и если есть, то должен быть выведен индекс элемента. 

int main()
{
	setlocale(LC_ALL, "Rus");

	srand(time(NULL));

	const int SIZE = 20;
	int arr[SIZE];
	int a;

	for (int i = 0; i < SIZE; i++)
	{
		arr[i] = rand() % 100;
	}

	for (int i = 0; i < SIZE; i++)
	{
		cout << arr[i] << endl;
	}

	cout << "Введите число от 0 до 100" << endl;

	cin >> a;

	for (int i = 0; i < SIZE; i++)
	{
		if (a == arr[i])
        cout << "Индекс элемента в массиве: " << i << endl;
	}
}