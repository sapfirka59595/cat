﻿#include <iostream>
using namespace std;

// 1. Составить программу, которая в зависимости от порядкового номера дня недели (1, 2, …, 7) выводит на экран его название (понедельник, вторник, …, воскресенье)

int main()
{
	setlocale(LC_ALL, "Rus");

	int a;

	cout << "Введите порядковый номер дня недели" << endl;

	cin >> a;

	switch (a)
	{
	case 1:
		cout << "Это понедельник" << endl;
		break;

	case 2:
		cout << "Это вторник" << endl;
		break;	

	case 3:
		cout << "Это среда" << endl;
		break;	

	case 4:
		cout << "Это четверг" << endl;
		break;

	case 5:
		cout << "Это пятница" << endl;
		break;

	case 6:
		cout << "Это суббота" << endl;
		break;

	case 7:
		cout << "Это воскресенье" << endl;
		break;

	default: 
		cout << "В неделе 7 дней. Неверное число" << endl;
		break;
	}
}